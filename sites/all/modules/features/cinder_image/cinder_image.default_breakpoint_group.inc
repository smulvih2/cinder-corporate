<?php

/**
 * @file
 * cinder_image.default_breakpoint_group.inc
 */

/**
 * Implements hook_default_breakpoint_group().
 */
function cinder_image_default_breakpoint_group() {
  $export = array();

  $breakpoint_group = new stdClass();
  $breakpoint_group->disabled = FALSE; /* Edit this to true to make a default breakpoint_group disabled initially */
  $breakpoint_group->api_version = 1;
  $breakpoint_group->machine_name = 'banner';
  $breakpoint_group->name = 'Banner';
  $breakpoint_group->breakpoints = array(
    0 => NULL,
    1 => NULL,
    2 => 'custom.user.large',
    3 => 'custom.user.medium',
    4 => 'custom.user.small',
  );
  $breakpoint_group->type = 'custom';
  $breakpoint_group->overridden = 0;
  $export['banner'] = $breakpoint_group;

  return $export;
}
