<?php

/**
 * @file
 * cinder_image.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cinder_image_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "breakpoints" && $api == "default_breakpoint_group") {
    return array("version" => "1");
  }
  if ($module == "breakpoints" && $api == "default_breakpoints") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function cinder_image_image_default_styles() {
  $styles = array();

  // Exported image style: banner_-_1024_x_720.
  $styles['banner_-_1024_x_720'] = array(
    'label' => 'Banner - 1024 x 350',
    'effects' => array(
      8 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1024,
          'height' => 350,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: banner_-_1440_x_650.
  $styles['banner_-_1440_x_650'] = array(
    'label' => 'Banner - 1440 x 350',
    'effects' => array(
      9 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1440,
          'height' => 350,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: banner_-_640_x_900.
  $styles['banner_-_640_x_900'] = array(
    'label' => 'Banner - 640 x 500',
    'effects' => array(
      10 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 640,
          'height' => 500,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
