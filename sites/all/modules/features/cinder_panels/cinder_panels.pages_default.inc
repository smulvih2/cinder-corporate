<?php

/**
 * @file
 * cinder_panels.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function cinder_panels_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'homepage';
  $page->task = 'page';
  $page->admin_title = 'Homepage';
  $page->admin_description = '';
  $page->path = 'homepage';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_homepage__panel';
  $handler->task = 'page';
  $handler->subtask = 'homepage';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Homepage',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
    'panels_page_title_state' => 1,
    'panels_page_title' => 'Drupal Web Development - Ottawa Consulting | Cinder Systems Corp.',
    'metatag_panels' => array(
      'enabled' => 1,
      'metatags' => array(
        'title' => array(
          'value' => 'Ottawa Web Design | Cinder Systems Corp.',
        ),
        'description' => array(
          'value' => 'Open source experts providing web design services in Ottawa, Ontario. Specializing in Drupal 7 and Drupal 8 solution architecture, software design and implementation.',
        ),
        'og:type' => array(
          'value' => 'website',
        ),
        'og:title' => array(
          'value' => 'Drupal Web Development - Ottawa Consulting | Cinder Systems Corp.',
        ),
        'og:description' => array(
          'value' => 'Open source software development in Ottawa, Ontario. Specializing in Drupal 7 and Drupal 8 solution architecture, development and consulting.',
        ),
      ),
    ),
  );
  $display = new panels_display();
  $display->layout = 'foundation_2col_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '622621e1-d350-4f98-9adf-b9036b33fed1';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_homepage__panel';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-c83359a8-1c28-48ef-9cea-22fb7aeeca26';
  $pane->panel = 'top';
  $pane->type = 'block';
  $pane->subtype = 'cinder_blocks-homepage_banner';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'c83359a8-1c28-48ef-9cea-22fb7aeeca26';
  $display->content['new-c83359a8-1c28-48ef-9cea-22fb7aeeca26'] = $pane;
  $display->panels['top'][0] = 'new-c83359a8-1c28-48ef-9cea-22fb7aeeca26';
  $pane = new stdClass();
  $pane->pid = 'new-c8a9138b-37f7-462a-acc9-50b5a3fa8be9';
  $pane->panel = 'top';
  $pane->type = 'page_messages';
  $pane->subtype = 'page_messages';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'c8a9138b-37f7-462a-acc9-50b5a3fa8be9';
  $display->content['new-c8a9138b-37f7-462a-acc9-50b5a3fa8be9'] = $pane;
  $display->panels['top'][1] = 'new-c8a9138b-37f7-462a-acc9-50b5a3fa8be9';
  $pane = new stdClass();
  $pane->pid = 'new-d5ed2ddd-dbff-465d-a1a4-7e012c294e07';
  $pane->panel = 'top';
  $pane->type = 'block';
  $pane->subtype = 'cinder_blocks-homepage_image_right';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'd5ed2ddd-dbff-465d-a1a4-7e012c294e07';
  $display->content['new-d5ed2ddd-dbff-465d-a1a4-7e012c294e07'] = $pane;
  $display->panels['top'][2] = 'new-d5ed2ddd-dbff-465d-a1a4-7e012c294e07';
  $pane = new stdClass();
  $pane->pid = 'new-a6cfdf55-46dd-4b0b-a351-d7870e7e8058';
  $pane->panel = 'top';
  $pane->type = 'block';
  $pane->subtype = 'cinder_blocks-technology_icon_banner';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'a6cfdf55-46dd-4b0b-a351-d7870e7e8058';
  $display->content['new-a6cfdf55-46dd-4b0b-a351-d7870e7e8058'] = $pane;
  $display->panels['top'][3] = 'new-a6cfdf55-46dd-4b0b-a351-d7870e7e8058';
  $pane = new stdClass();
  $pane->pid = 'new-ba71e9b3-be88-4446-b2a4-69c69f27d556';
  $pane->panel = 'top';
  $pane->type = 'block';
  $pane->subtype = 'cinder_blocks-homepage_image_left';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = 'ba71e9b3-be88-4446-b2a4-69c69f27d556';
  $display->content['new-ba71e9b3-be88-4446-b2a4-69c69f27d556'] = $pane;
  $display->panels['top'][4] = 'new-ba71e9b3-be88-4446-b2a4-69c69f27d556';
  $pane = new stdClass();
  $pane->pid = 'new-99d445eb-f971-4ff9-a5a6-99f21ef31ada';
  $pane->panel = 'top';
  $pane->type = 'block';
  $pane->subtype = 'cinder_blocks-homepage_image_right_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 5;
  $pane->locks = array();
  $pane->uuid = '99d445eb-f971-4ff9-a5a6-99f21ef31ada';
  $display->content['new-99d445eb-f971-4ff9-a5a6-99f21ef31ada'] = $pane;
  $display->panels['top'][5] = 'new-99d445eb-f971-4ff9-a5a6-99f21ef31ada';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['homepage'] = $page;

  return $pages;

}
