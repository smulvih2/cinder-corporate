<?php

/**
 * @file
 * cinder_variable.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cinder_variable_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
