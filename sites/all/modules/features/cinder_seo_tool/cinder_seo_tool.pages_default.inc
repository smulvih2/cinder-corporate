<?php

/**
 * @file
 * cinder_seo_tool.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function cinder_seo_tool_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'free_seo_analysis_tool';
  $page->task = 'page';
  $page->admin_title = 'Free SEO Analysis Tool';
  $page->admin_description = '';
  $page->path = 'free-seo-analysis-tool';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_free_seo_analysis_tool__panel';
  $handler->task = 'page';
  $handler->subtask = 'free_seo_analysis_tool';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'SEO Analysis',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'cinder-seo-analysis',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
    'metatag_panels' => array(
      'enabled' => 1,
      'metatags' => array(
        'title' => array(
          'value' => 'Free SEO Analysis Tool | [site:name]',
        ),
        'description' => array(
          'value' => 'Free website analysis tool, great for monitoring website performance, Search Engine Optimization (SEO) factors, domain authority, and other best practices.',
        ),
      ),
    ),
  );
  $display = new panels_display();
  $display->layout = 'foundation_1col';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '5189563b-3e8a-426a-85d4-0af98897861f';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_free_seo_analysis_tool__panel';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-a99cfe17-0d5b-4023-b05e-1d16357979c5';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'cinder_seo-cinder_seo_search_form';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '%title',
    'override_title_heading' => 'h1',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'a99cfe17-0d5b-4023-b05e-1d16357979c5';
  $display->content['new-a99cfe17-0d5b-4023-b05e-1d16357979c5'] = $pane;
  $display->panels['middle'][0] = 'new-a99cfe17-0d5b-4023-b05e-1d16357979c5';
  $pane = new stdClass();
  $pane->pid = 'new-571e1d4e-9a1f-48c4-a48a-35787b422971';
  $pane->panel = 'middle';
  $pane->type = 'page_messages';
  $pane->subtype = 'page_messages';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '571e1d4e-9a1f-48c4-a48a-35787b422971';
  $display->content['new-571e1d4e-9a1f-48c4-a48a-35787b422971'] = $pane;
  $display->panels['middle'][1] = 'new-571e1d4e-9a1f-48c4-a48a-35787b422971';
  $pane = new stdClass();
  $pane->pid = 'new-650f52cd-8583-4709-8661-2393a44c2e1c';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'cinder_seo-cinder_seo_results';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => 'cinder-seo-results-anchor',
    'css_class' => '',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '650f52cd-8583-4709-8661-2393a44c2e1c';
  $display->content['new-650f52cd-8583-4709-8661-2393a44c2e1c'] = $pane;
  $display->panels['middle'][2] = 'new-650f52cd-8583-4709-8661-2393a44c2e1c';
  $pane = new stdClass();
  $pane->pid = 'new-e6050f9d-8f93-428b-9043-543a3bfb3fe6';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'cinder_blocks-seo_tool_share_block';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'e6050f9d-8f93-428b-9043-543a3bfb3fe6';
  $display->content['new-e6050f9d-8f93-428b-9043-543a3bfb3fe6'] = $pane;
  $display->panels['middle'][3] = 'new-e6050f9d-8f93-428b-9043-543a3bfb3fe6';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-a99cfe17-0d5b-4023-b05e-1d16357979c5';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['free_seo_analysis_tool'] = $page;

  return $pages;

}
