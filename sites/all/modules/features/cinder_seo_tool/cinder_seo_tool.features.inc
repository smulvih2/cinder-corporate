<?php

/**
 * @file
 * cinder_seo_tool.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cinder_seo_tool_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}
