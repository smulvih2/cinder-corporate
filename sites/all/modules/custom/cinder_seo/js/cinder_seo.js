(function ($) {

  Drupal.behaviors.cinderSEO = {
    attach: function (context, settings) {

      /*$(context).find(".pane-cinder-seo-cinder-seo-results").once("cinder-seo", function () {
        google.charts.load('current', {'packages':['gauge']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

          $('.seo-chart').each(function(index, value) {
            var options = {
              width: 150, height: 150,
              redFrom: 0, redTo: 55,
              yellowFrom:55, yellowTo: 80,
              greenFrom: 80, greenTo: 100,
              minorTicks: 5,
              majorTicks: [0,10,20,30,40,50,60,70,80,90,100],
            };

            var score = $(this).data('value');

            var data = google.visualization.arrayToDataTable([
              ['Label', 'Value'],
              ['', 0],
            ]);

            var chart = new google.visualization.Gauge($(this)[0]);

            setTimeout(function() {
              data.setValue(0, 1, score);
              chart.draw(data, options);
            }, 500);

            chart.draw(data, options);
          });

        }
      });*/

      var opts = {
        fontSize: '12px',
        angle: 0.35,
        lineWidth: 0.1,
        radiusScale: 1,
        limitMax: false,
        limitMin: false,
        strokeColor: '#EEEEEE',
        generateGradient: true,
        highDpiSupport: true,
      };

      if ($('#canvas-cinder-score').length) {
        // Go to results anchor
        window.location.hash = "#cinder-seo-results-anchor";

        opts.colorStart = set_gauge_color(Drupal.settings.cinder_seo.cinder_score);
        opts.colorStop = set_gauge_color(Drupal.settings.cinder_seo.cinder_score);
        var target = document.getElementById('canvas-cinder-score');
        var gauge = new Donut(target).setOptions(opts);
        gauge.maxValue = 100;
        gauge.setMinValue(0);
        gauge.animationSpeed = 32;
        gauge.set(Drupal.settings.cinder_seo.cinder_score);
        gauge.setTextField(document.getElementById("canvas-cinder-score-label"));
        $('#canvas-cinder-score-label').addClass('live');
      }

      if ($('#canvas-mobile-performance').length) {
        opts.colorStart = set_gauge_color(Drupal.settings.cinder_seo.mobile);
        opts.colorStop = set_gauge_color(Drupal.settings.cinder_seo.mobile);
        var target = document.getElementById('canvas-mobile-performance');
        var gauge = new Donut(target).setOptions(opts);
        gauge.maxValue = 100;
        gauge.setMinValue(0);
        gauge.animationSpeed = 32;
        gauge.set(Drupal.settings.cinder_seo.mobile);
        gauge.setTextField(document.getElementById("canvas-mobile-performance-label"));
        $('#canvas-mobile-performance-label').addClass('live');
      }

      if ($('#canvas-desktop-performance').length) {
        setTimeout(function() {
          opts.colorStart = set_gauge_color(Drupal.settings.cinder_seo.desktop);
          opts.colorStop = set_gauge_color(Drupal.settings.cinder_seo.desktop);
          var target = document.getElementById('canvas-desktop-performance');
          var gauge = new Donut(target).setOptions(opts);
          gauge.maxValue = 100;
          gauge.setMinValue(0);
          gauge.animationSpeed = 32;
          gauge.set(Drupal.settings.cinder_seo.desktop);
          gauge.setTextField(document.getElementById("canvas-desktop-performance-label"));
          $('#canvas-desktop-performance-label').addClass('live');
        }, 500);
      }

      if ($('#canvas-seo').length) {
        setTimeout(function() {
          opts.colorStart = set_gauge_color(Drupal.settings.cinder_seo.seo);
          opts.colorStop = set_gauge_color(Drupal.settings.cinder_seo.seo);
          var target = document.getElementById('canvas-seo');
          var gauge = new Donut(target).setOptions(opts);
          gauge.maxValue = 100;
          gauge.setMinValue(0);
          gauge.animationSpeed = 32;
          gauge.set(Drupal.settings.cinder_seo.seo);
          gauge.setTextField(document.getElementById("canvas-seo-label"));
          $('#canvas-seo-label').addClass('live');
        }, 1000);
      }

      if ($('#canvas-accessibility').length) {
        setTimeout(function() {
          opts.colorStart = set_gauge_color(Drupal.settings.cinder_seo.accessibility);
          opts.colorStop = set_gauge_color(Drupal.settings.cinder_seo.accessibility);
          var target = document.getElementById('canvas-accessibility');
          var gauge = new Donut(target).setOptions(opts);
          gauge.maxValue = 100;
          gauge.setMinValue(0);
          gauge.animationSpeed = 32;
          gauge.set(Drupal.settings.cinder_seo.accessibility);
          gauge.setTextField(document.getElementById("canvas-accessibility-label"));
          $('#canvas-accessibility-label').addClass('live');
        }, 1500);
      }

      if ($('#canvas-best-practices').length) {
        setTimeout(function() {
          opts.colorStart = set_gauge_color(Drupal.settings.cinder_seo.best_practices);
          opts.colorStop = set_gauge_color(Drupal.settings.cinder_seo.best_practices);
          var target = document.getElementById('canvas-best-practices');
          var gauge = new Donut(target).setOptions(opts);
          gauge.maxValue = 100;
          gauge.setMinValue(0);
          gauge.animationSpeed = 32;
          gauge.set(Drupal.settings.cinder_seo.best_practices);
          gauge.setTextField(document.getElementById("canvas-best-practices-label"));
          $('#canvas-best-practices-label').addClass('live');
        }, 2000);
      }

      // Vertical tabs
      $('.tabs').tabs();

      $('.tabs ul li a').on('click', function(e) {
        e.preventDefault();
      });

      function set_gauge_color(score) {
        if (score < 50) {
          return '#f04124';
        }
        else if (score < 80) {
          return '#f8a32d';
        }
        else {
          return '#43AC6A';
        }
      }

    }
  };
})(jQuery);
