<?php

/**
 * Implements template_preprocess_html().
 */
function cinder_preprocess_html(&$variables) {
	$variables['noindex'] = FALSE;

	if (arg(0) == 'user') {
		if (!is_numeric(arg(1))) {
			$variables['noindex'] = TRUE;
		}
	}
}

/**
 * Implements template_preprocess_page.
 */
function cinder_preprocess_page(&$variables) {
	$breakpoints = picture_get_mapping_breakpoints(picture_mapping_load('banner'));

	// Page content type
	if (!empty($variables['node']) && $variables['node']->type == 'page') {
		if (!empty($variables['node']->field_banner_image[LANGUAGE_NONE][0]['uri'])) {
			$image_uri = $variables['node']->field_banner_image[LANGUAGE_NONE][0]['uri'];
			$image_alt = $variables['node']->field_banner_image[LANGUAGE_NONE][0]['alt'];
			$image_title = $variables['node']->field_banner_image[LANGUAGE_NONE][0]['title'];
			$node_title = $variables['node']->title;
			$title_prefix = '';

			if (!empty($variables['node']->field_title_prefix[LANGUAGE_NONE][0]['value'])) {
				$title_prefix = $variables['node']->field_title_prefix[LANGUAGE_NONE][0]['value'];
			}

			$banner_image = theme('picture', array(
				'style_name' => 'banner_-_1024_x_720',
				'uri' => $image_uri,
				'height' => '350px',
				'width' => '1440px',
				'breakpoints' => $breakpoints,
				'alt' => $image_alt,
				'title' => $image_title,
			));

			$banner_image_output = '<div id="cinder-banner-image">' .
																'<div class="banner-wrapper">' .
																	$banner_image .
																	'<div class="banner-text-wrapper">
																		<div class="title-prefix">' . $title_prefix . '</div>
																		<h1>' . $node_title . '</h1>
																	</div>
																</div>
																<div class="banner-color-overlay"></div>
															</div>';
		}
		else {
			$title = $node_title = $variables['node']->title;

			$banner_image_output = '<div id="cinder-banner-image">' .
																'<div class="banner-wrapper">
																	<div class="banner-text-wrapper">
																		<div class="">' . $title_prefix . '</div>
																		<h1>' . $node_title . '</h1>
																	</div>
																</div>
															</div>';
		}

		$variables['banner_image'] = $banner_image_output;
	}

	// Blog content type
	if (!empty($variables['node']) && $variables['node']->type == 'blog') {
		$banner_image = '';

		if (!empty($variables['node']->field_main_blog_image[LANGUAGE_NONE][0]['uri'])) {
			$image_uri = $variables['node']->field_main_blog_image[LANGUAGE_NONE][0]['uri'];
			$image_alt = $variables['node']->field_main_blog_image[LANGUAGE_NONE][0]['alt'];
			$image_title = $variables['node']->field_main_blog_image[LANGUAGE_NONE][0]['title'];

			$banner_image = theme('picture', array(
				'style_name' => 'banner_-_1024_x_720',
				'uri' => $image_uri,
				'height' => '350px',
				'width' => '1440px',
				'breakpoints' => $breakpoints,
				'alt' => $image_alt,
				'title' => $image_title,
			));
		}

		$variables['banner_image'] = $banner_image;

		$title_prefix = '';

		if (!empty($variables['node']->field_title_prefix[LANGUAGE_NONE][0]['value'])) {
			$title_prefix_value = $variables['node']->field_title_prefix[LANGUAGE_NONE][0]['value'];
			$title_prefix = '<div class="title-prefix">' . $title_prefix_value . '</div>';
		}

		$variables['title_prefix'] = $title_prefix;
	}
}

/**
 * Implements template_preprocess_node.
 */
function cinder_preprocess_node(&$variables) {
	if (!empty($variables['node']) && $variables['node']->type == 'blog') {
		$author_uid = $variables['node']->uid;
		$user_data = user_load($author_uid);

		if (!empty($user_data->field_full_name[LANGUAGE_NONE][0]['value'])) {
			$variables['user_full_name'] = $user_data->field_full_name[LANGUAGE_NONE][0]['value'];
		}

		if (!empty($user_data->field_title_line[LANGUAGE_NONE][0]['value'])) {
			$variables['user_title_line'] = $user_data->field_title_line[LANGUAGE_NONE][0]['value'];
		}

		$variables['date_created'] = date('F j, Y', $variables['node']->created);

		if (!empty($variables['node']->field_read_time[LANGUAGE_NONE][0]['value'])) {
			$read_time = $variables['node']->field_read_time[LANGUAGE_NONE][0]['value'];
		}
		else {
			$read_time = '';
		}

		$variables['read_time'] = $read_time;
	}
}
