(function ($) {

  $(document).ready(function() {
    // Add class to off-canvas menu to get styling from Zurb
    $('.left-off-canvas-menu #main-menu').addClass('off-canvas-list');

    // Back to top of page
    $(window).scroll(function() {
      if ($(this).scrollTop() > 200) {
        $('.back-to-top:hidden').stop(true, true).fadeIn();
      }else{
        $('.back-to-top').stop(true, true).fadeOut();
      }
    });

    $(".back-to-top a").click(function () {
      $('html, body').animate({scrollTop : 0},800);
      return false;
    });

    // Make equalize work once elements become stacked
    $(document).foundation({
      equalizer : {
        equalize_on_stack: true
      }
    });

    // Hide menu
    $(window).scroll(function(event){
      if ($(this).scrollTop() > 300) {
        $('.top-bar').addClass('color-bar');
      }
      else {
        $('.top-bar').removeClass('color-bar');
      }
    });

    // AOS Init
    AOS.init();

  });

})(jQuery);
