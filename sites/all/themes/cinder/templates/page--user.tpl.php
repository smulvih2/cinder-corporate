<div class="off-canvas-wrap" data-offcanvas>

  <aside class="left-off-canvas-menu off-canvas-menu">
    <div class="inner-wrapper">
      <div class="header">
        <div class="right">
          <a class="exit-off-canvas">CLOSE</a>
        </div>
      </div>
      <?php if ($top_bar_main_menu) : ?>
        <?php print $top_bar_main_menu; ?>
      <?php endif; ?>
    </div>
  </aside>

  <header class="l-header">
    <div class="top-bar" data-topbar <?php print $top_bar_options; ?>>
      <div class="box-left">
        <div class="toggle-topbar menu-icon">
          <a href="#" class="left-off-canvas-toggle">
            <span><?php print $top_bar_menu_text; ?></span>
          </a>
        </div>
      </div>

      <div class="box-right">
        <span class="login-link">
          <?php print $user->uid ? l('Logout', 'user/logout') : l('Login', 'user/login'); ?>
        </span>
      </div>
    </div>

    <?php if ($alt_header): ?>
      <section class="row <?php print $alt_header_classes; ?>">

        <?php if ($linked_logo): print $linked_logo; endif; ?>

        <?php if ($site_name): ?>
          <?php if ($title): ?>
            <div id="site-name" class="element-invisible">
              <strong>
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
              </strong>
            </div>
          <?php else: ?>
            <h1 id="site-name">
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
            </h1>
          <?php endif; ?>
        <?php endif; ?>

        <?php if ($site_slogan): ?>
          <h2 title="<?php print $site_slogan; ?>" class="site-slogan"><?php print $site_slogan; ?></h2>
        <?php endif; ?>

        <?php if ($alt_main_menu): ?>
          <nav id="main-menu" class="navigation" role="navigation">
            <?php print ($alt_main_menu); ?>
          </nav>
        <?php endif; ?>

        <?php if ($alt_secondary_menu): ?>
          <nav id="secondary-menu" class="navigation" role="navigation">
            <?php print $alt_secondary_menu; ?>
          </nav>
        <?php endif; ?>

      </section>
    <?php endif; ?>

    <?php if (!empty($page['header'])): ?>
      <section class="l-header-region row">
        <div class="columns">
          <?php print render($page['header']); ?>
        </div>
      </section>
    <?php endif; ?>

  </header>

  <div class="inner-wrap">
    <div role="document" class="page">

      <?php if (!empty($page['featured'])): ?>
        <section class="l-featured row">
          <div class="columns">
            <?php print render($page['featured']); ?>
          </div>
        </section>
      <?php endif; ?>

      <?php if (!empty($page['help'])): ?>
        <section class="l-help row">
          <div class="columns">
            <?php print render($page['help']); ?>
          </div>
        </section>
      <?php endif; ?>

      <main class="row l-main">
        <!-- .l-main region -->
        <div class="<?php print $main_grid; ?> main columns">
          <?php if (!empty($page['highlighted'])): ?>
            <div class="highlight panel callout">
              <?php print render($page['highlighted']); ?>
            </div>
          <?php endif; ?>

          <a id="main-content"></a>

          <div id="cinder-login-header"></div>

          <div id="cinder-login-wrapper">
            <?php if ($title): ?>
              <?php print render($title_prefix); ?>
              <h1 id="page-title" class="title"><?php print $title; ?></h1>
              <?php print render($title_suffix); ?>
            <?php endif; ?>

            <?php if ($messages && !$zurb_foundation_messages_modal): ?>
              <section class="l-messages row">
                <div class="columns">
                  <?php if ($messages): print $messages; endif; ?>
                </div>
              </section>
            <?php endif; ?>

            <?php if (!empty($tabs)): ?>
              <?php print render($tabs); ?>
              <?php if (!empty($tabs2)): print render($tabs2); endif; ?>
            <?php endif; ?>

            <?php if ($action_links): ?>
              <ul class="action-links">
                <?php print render($action_links); ?>
              </ul>
            <?php endif; ?>

            <?php print render($page['content']); ?>
          </div>
        </div>

        <?php if (!empty($page['sidebar_first'])): ?>
          <aside role="complementary" class="<?php print $sidebar_first_grid; ?> sidebar-first columns sidebar">
            <?php print render($page['sidebar_first']); ?>
          </aside>
        <?php endif; ?>

        <?php if (!empty($page['sidebar_second'])): ?>
          <aside role="complementary" class="<?php print $sidebar_sec_grid; ?> sidebar-second columns sidebar">
            <?php print render($page['sidebar_second']); ?>
          </aside>
        <?php endif; ?>
      </main>

      <?php if (!empty($page['triptych_first']) || !empty($page['triptych_middle']) || !empty($page['triptych_last'])): ?>
        <section class="l-triptych row">
          <div class="triptych-first medium-4 columns">
            <?php print render($page['triptych_first']); ?>
          </div>
          <div class="triptych-middle medium-4 columns">
            <?php print render($page['triptych_middle']); ?>
          </div>
          <div class="triptych-last medium-4 columns">
            <?php print render($page['triptych_last']); ?>
          </div>
        </section>
      <?php endif; ?>
      
      <div class="cinder-systems-logo">
        <img src="/sites/all/themes/cinder/images/Government consulting Ottawa - Cinder Systems Corp logo.png" alt="Cinder Systems Corp. logo"/>
      </div>

      <footer class="l-footer panel">
        <div class="color-top"></div>

        <div class="cinder-container row">
          <div class="footer-1 columns small-12 medium-4 large-3 left">
            <img src="/sites/all/themes/cinder/images/Cinder Logo white.png" alt="Cinder Systems Corp. logo"/>
          </div>

          <?php if (!empty($page['footer_firstcolumn'])): ?>
            <div class="footer-2 columns small-12 medium-4 large-3 left">
              <?php print render($page['footer_firstcolumn']); ?>
            </div>
          <?php endif; ?>

          <?php if (!empty($page['footer_secondcolumn'])): ?>
            <div class="footer-3 columns small-12 medium-4 large-3 left">
              <?php print render($page['footer_secondcolumn']); ?>
            </div>
          <?php endif; ?>

          <?php if (!empty($page['footer_thirdcolumn'])): ?>
            <div class="footer-4 columns small-12 medium-4 large-3 left">
              <?php print render($page['footer_thirdcolumn']); ?>
            </div>
          <?php endif; ?>

          <?php if (!empty($page['footer_fourthcolumn'])): ?>
            <div class="footer-5 columns small-12 medium-4 large-3 left">
              <?php print render($page['footer_fourthcolumn']); ?>
            </div>
          <?php endif; ?>
        </div>

        <?php if ($site_name) : ?>
          <div class="copyright columns">
            &copy; <?php print date('Y') . ' ' . $site_name . ' ' . t('All rights reserved.'); ?>
          </div>
        <?php endif; ?>
      </footer>

      <?php if ($messages && $zurb_foundation_messages_modal): print $messages; endif; ?>
    </div>

    <a class="exit-off-canvas page"></a>

    <div class="back-to-top">
      <a href="#">
        <img title="Back to Top" alt="Click here to be brought to the top of the page" src="/sites/all/themes/cinder/images/back-to-top.png" />
      </a>
    </div>

  </div>
</div>
