<div class="inner-wrap">
  <div role="document" class="page">

    <main class="row l-main">
      <div class="<?php print $main_grid; ?> main columns">
        <a id="main-content"></a>

        <div id="cinder-batch-api-wrapper">
          <?php if ($title): ?>
            <?php print render($title_prefix); ?>
            <h1 id="page-title" class="title"><?php print $title; ?></h1>
            <?php print render($title_suffix); ?>
          <?php endif; ?>

          <?php print render($page['content']); ?>
        </div>
      </div>
    </main>
    
    <div class="cinder-systems-logo">
      <img src="/sites/all/themes/cinder/images/Government consulting Ottawa - Cinder Systems Corp logo.png" alt="Cinder Systems Corp. logo"/>
    </div>

    <footer class="l-footer panel">
      <div class="color-top"></div>

      <div class="cinder-container row">
        <div class="footer-1 columns small-12 medium-4 large-3 left">
          <img src="/sites/all/themes/cinder/images/Cinder Logo white.png" alt="Cinder Systems Corp. logo"/>
        </div>

        <?php if (!empty($page['footer_firstcolumn'])): ?>
          <div class="footer-2 columns small-12 medium-4 large-3 left">
            <?php print render($page['footer_firstcolumn']); ?>
          </div>
        <?php endif; ?>

        <?php if (!empty($page['footer_secondcolumn'])): ?>
          <div class="footer-3 columns small-12 medium-4 large-3 left">
            <?php print render($page['footer_secondcolumn']); ?>
          </div>
        <?php endif; ?>

        <?php if (!empty($page['footer_thirdcolumn'])): ?>
          <div class="footer-4 columns small-12 medium-4 large-3 left">
            <?php print render($page['footer_thirdcolumn']); ?>
          </div>
        <?php endif; ?>

        <?php if (!empty($page['footer_fourthcolumn'])): ?>
          <div class="footer-5 columns small-12 medium-4 large-3 left">
            <?php print render($page['footer_fourthcolumn']); ?>
          </div>
        <?php endif; ?>
      </div>

      <?php if ($site_name) : ?>
        <div class="copyright columns">
          &copy; <?php print date('Y') . ' ' . $site_name . ' ' . t('All rights reserved.'); ?>
        </div>
      <?php endif; ?>
    </footer>

    <?php if ($messages && $zurb_foundation_messages_modal): print $messages; endif; ?>
  </div>

  <a class="exit-off-canvas page"></a>

</div>
