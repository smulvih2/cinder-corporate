<?php

/**
 * Implements hook_panels_layouts()
 */
function ottawa_ottawa_2col_stacked_panels_layouts() {

  $items['ottawa_2col_stacked'] = array(
    'title' => t('Ottawa: stacked 2 column row'),
    'category' => t('Ottawa'),
    'icon' => 'ottawa_2col_stacked.png',
    'theme' => 'ottawa_2col_stacked',
    //'admin css' => '../foundation_panels_admin.css',
    'regions' => array(
      'banner' => t('Banner'),
      'top' => t('Top'),
      'left' => t('Left'),
      'right' => t('Right'),
      'bottom' => t('Bottom'),
    ),
  );

  return $items;
}

