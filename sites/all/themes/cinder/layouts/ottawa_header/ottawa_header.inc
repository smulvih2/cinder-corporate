<?php

/**
 * Implements hook_panels_layouts()
 */
function ottawa_ottawa_header_panels_layouts() {
  $items['ottawa_header'] = array(
    'title' => t('Ottawa Header: 4 column row'),
    'category' => t('Ottawa'),
    'icon' => 'ottawa_header.png',
    'theme' => 'ottawa_header',
    //'admin css' => '../foundation_panels_admin.css',
    'regions' => array(
      'left' => t('Left'),
      'middle_left' => t('Middle Top'),
      'middle_right' => t('Middle Bottom'),
      'right' => t('Right')
    ),
  );

  return $items;
}

