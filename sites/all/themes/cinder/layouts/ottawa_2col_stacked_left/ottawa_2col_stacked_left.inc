<?php

/**
 * Implements hook_panels_layouts()
 */
function ottawa_ottawa_2col_stacked_left_panels_layouts() {

  $items['ottawa_2col_stacked_left'] = array(
    'title' => t('Ottawa: stacked 2 column row left'),
    'category' => t('Ottawa'),
    'icon' => 'ottawa_2col_stacked_left.png',
    'theme' => 'ottawa_2col_stacked_left',
    //'admin css' => '../foundation_panels_admin.css',
    'regions' => array(
      'top' => t('Top'),
      'left' => t('Left'),
      'right' => t('Right'),
      'bottom' => t('Bottom'),
    ),
  );

  return $items;
}

